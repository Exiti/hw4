package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.data.Book;
import com.example.MyBookShopApp.data.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MainPageController {

    private final BookService bookService;

    @Autowired
    public MainPageController(BookService bookService) {
        this.bookService = bookService;
    }

    @ModelAttribute("recommendedBooks")
    public List<Book> recommendedBooks(){
        return bookService.getBooksData();
    }

    @GetMapping("/")
    public String mainPage(){
        return "index";
    }
    @GetMapping("/index")
    public String mainPage2() {
        return "index";
    }
    @GetMapping("/postponed")
    public String getPostponed() {
        return "postponed";
    }
    @GetMapping("/genres")
    public String getGenres() {
        return "genres/index";
    }
    @GetMapping("/cart")
    public String getCar() {
        return "cart";
    }
    @GetMapping("/signin")
    public String signin() {
        return "signin";
    }
    @GetMapping("/search")
    public String getSearch() {
        return "search/index";
    }
    @GetMapping("/documents")
    public String getDocuments() {
        return "documents/index";
    }
    @GetMapping("/about")
    public String about() {
        return "about";
    }
    @GetMapping("/faq")
    public String faq() {
        return "faq";
    }
    @GetMapping("/contacts")
    public String contacts() {
        return "contacts";
    }



}
